# Ultrasonic autopilot
# Anton Fedyashov
# Denis Teplyukov
# Daniil Nepryahin
# 2017, MMCS

import serial
import time
from re import match,split

Run = 1
SENSORS_TIMEOUT = 10

# Variable for storing last movement of car:
# 0 - nothing 
# 1 - forward
# 2 - left
# 3 - right
# 4 - halt
LastMove = 0
current_time = time.time()

print('Initializing COM-ports')
try:
    # Sensors port - Arduino Mega
    port_sen = serial.Serial('/dev/ttyUSB0')
    time.sleep(0.5)
    port_sen.baudrate = 19200
    time.sleep(0.5)
    port_sen.timeout = 0

    # Chassis port - Arduino UNO
    port_ch = serial.Serial('/dev/ttyACM0')
    time.sleep(0.5)
    port_ch.baudrate = 19200
    time.sleep(0.5)
    port_ch.timeout = 0

except serial.SerialException:
    print("Serial failure")

# Delay to wait for car initialization
time.sleep(15)

print('Starting autopilot')
trash = port_sen.readline()
port_sen.reset_input_buffer()
time.sleep(1)

port_ch.write(b'START\r\n')

while Run:

    port_ch.reset_output_buffer()
    port_ch.reset_input_buffer()
    port_sen.reset_input_buffer()

    chassis = port_ch.readline()
    sensor = port_sen.readline()

    # Watchdog subsystem
    if chassis == b'ASK\r\n':
       port_ch.write(b'ANSWER\r\n')
    
    # Sensors watchdog system
    check_time = time.time()
    diff = check_time - current_time
    if diff > SENSORS_TIMEOUT:
        Run = 0
        port_ch.write(b'HALT\r\n')
        port_ch.write(b'FAILSEN\r\n')
        continue

    if len(sensor) == 0:
        continue
    current_time = time.time()

    # String for storing result of reg.exp. processing result
    sensorstr=""

    # Converting from bytes to string
    sensor = str(sensor,'utf-8')
 
    # Parsing data from input string
    if match(r'D.....',sensor):
        sensor=split(r'D',sensor) 
        raw=split(r'/',sensor[1])
        sensor=raw[0]
        dist=raw[1]
        dist=split(r',',dist)

        # Debug
        print(dist[0] + ' ' + dist[1] + ' ' + dist[2] + ' ' + dist[3] + ' ' + dist[4]) 

# Regular expressions processing
# CENTER
        if match(r'.000.',sensor):
            sensorstr=sensorstr+'1'
        else:
            sensorstr=sensorstr+'0'
     
# LEFT
        left=match(r'00...',sensor)
        if left:
            sensorstr=sensorstr+'1'
        else:
            sensorstr=sensorstr+'0'

# RIGHT
        right=match(r'...00',sensor)
        if  right:
            sensorstr=sensorstr+'1'
        else:
            sensorstr=sensorstr+'0'
 
# HALT
        halt1=match(r'.1.1.',sensor)
        halt2=match(r'1..11',sensor)
        halt3=match(r'11..1',sensor)
        halt4=match(r'1.1.1',sensor)
        halt5=match(r'01.01',sensor)
        if  halt1 or halt2 or halt3 or halt4 or halt5:
            sensorstr=sensorstr+'1'
        else:
            sensorstr=sensorstr+'0'

    # Debug
    print(sensorstr)

# Making deсisions based on reg.exp. results:
    
    if sensorstr == '0010':
        port_ch.write(b'RIGHT\r\n')    
        port_ch.write(b'L60A\r\n')
        LastMove = 3

    elif sensorstr == '0100':
        port_ch.write(b'LEFT\r\n')
        port_ch.write(b'L60A\r\n')
        LastMove = 2

    elif sensorstr == '0110':
        left_dist = int(dist[0])
        right_dist = int(dist[4])
        if left_dist > right_dist:
             port_ch.write(b'LEFT\r\n')
             LastMove = 2  
        else:
             port_ch.write(b'RIGHT\r\n')
             LastMove = 3
        port_ch.write(b'L60A\r\n') 
    
    elif sensorstr in ['1000','1010','1100','1110']:
        # Direction flag
        to_left = 0

        # Aspect ratio of left and right ranges
        division = 0

        # Maximum aspect ratio
        border = 5

        left_dist = int(dist[0])
        right_dist = int(dist[4])
        port_ch.write(b'L65A\r\n')

        # Calculating aspect ratio
        if left_dist > right_dist:
             division = left_dist / right_dist
             to_left = -1
             LastMove = 2
        elif left_dist < right_dist:
             division = right_dist / left_dist
             to_left = 1
             LastMove = 3
 
        # Calculating shift from the middle
        delta = int(division * 50.0 / border)
        
        # Making value for command
        final_delta = 50 + to_left*delta

        command = 'S' + str(final_delta) + 'A\r\n'

        # Converting from string to bytes
        command = bytes(command,'utf-8') 
        port_ch.write(command)

    elif sensorstr in ['0001','0011','0101','0000']:
        # It's hard situation, reducing speed
        port_ch.write(b'L57A\r\n')

        left_dist = int(dist[0])
        mid_dist = int(dist[2])
        right_dist = int(dist[4])

        # Minimum range in cm for sensors
        min_border = 25

        # Calculating direction and checking range to obstacle
        if left_dist < right_dist:
            port_ch.write(b'RIGHT\r\n')
            LastMove = 3
            if left_dist < min_border or mid_dist < min_border:
                if LastMove != 4:
                    port_ch.write(b'HALT\r\n')
                    port_ch.write(b'FAILOBST\r\n')
                    LastMove = 4
        else:
            port_ch.write(b'LEFT\r\n')
            LastMove = 2
            if right_dist < min_border or mid_dist < min_border:
                if LastMove != 4:
                    port_ch.write(b'HALT\r\n')
                    port_ch.write(b'FAILOBST\r\n')
                    LastMove = 4
 
    sensorstr=""

port_ch.close()
port_sen.close()
